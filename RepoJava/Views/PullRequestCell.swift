//
//  PullRequestCell.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 08/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import UIKit
import Kingfisher

final class PullRequestCell: UITableViewCell {
    
    @IBOutlet weak var prTitle: UILabel!
    @IBOutlet weak var prDescription: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var username: UILabel!
    
    func configureImageCorners() {
        userImage.layer.cornerRadius = userImage.frame.height / 2.0
        userImage.layer.masksToBounds = true
    }
    
    func configureCell(with pullRequestState: PullRequest) {
        prTitle.text = pullRequestState.title!
        prDescription.text = pullRequestState.body!
        username.text = pullRequestState.user?  .login!
        
        let imageUrl = URL(string: (pullRequestState.user?.avatar_url!)!)
        userImage.kf.setImage(with: imageUrl)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureImageCorners()
    }
}
