//
//  RepositoryCell.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import UIKit
import Kingfisher

final class RepositoryCell: UITableViewCell {
    
    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var repositoryDescription: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var forkCount: UILabel!
    @IBOutlet weak var starCount: UILabel!
    
    func configureImageCorners() {
        userImage.layer.cornerRadius = userImage.frame.height / 2.0
        userImage.layer.masksToBounds = true

    }
    
    func configureCell(with repositoryState: Repository) {
        repositoryName.text = repositoryState.name ?? ""
        repositoryDescription.text = repositoryState.description ?? ""
        username.text = repositoryState.owner?.login ?? ""
        forkCount.text = "\(repositoryState.forks!)"
        starCount.text = "\(repositoryState.stargazers_count!)"
        
        let imageUrl = URL(string: (repositoryState.owner?.avatar_url!)!)
        userImage.kf.setImage(with: imageUrl)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureImageCorners()
    }
}
