//
//  GithubAPI.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import Foundation

final class GithubAPI {
    static func loadJavaRepositories(page: String, completion: @escaping (RepositoriesList?) -> Void) {
        let baseUrl = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)"
        let urlRequest: URLRequest = URLRequest(url: URL(string: baseUrl)!)
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest) { data, responsse, error in
            
            guard error == nil else {
                completion(nil)
                return
            }
            
            do {
               let repositories = try GithubAPI.parseJsonToRepositoriesArray(data!)
                DispatchQueue.main.async {
                    completion(repositories)
                }
            } catch {
                fatalError("Could not get data from the server")
            }
        }
        
        task.resume()
    }
    
    static func loadPullRequests(repositoryPath: String, completion: @escaping ([PullRequest]) -> Void) {
        let baseUrl = "https://api.github.com/repos/\(repositoryPath)/pulls?state=all"
        let urlRequest: URLRequest = URLRequest(url: URL(string: baseUrl)!)
        let session = URLSession.shared
        
        let task = session.dataTask(with: urlRequest) { data, responsse, error in
            
            guard error == nil else {
                completion([])
                return
            }
            
            do {
                let pullRequests = try GithubAPI.parseJsonToPullRequestsArray(data!)
                DispatchQueue.main.async {
                    completion(pullRequests)
                }
            } catch {
                fatalError("Could not get data from the server")
            }
        }
        
        task.resume()
    }
    
    static func parseJsonToRepositoriesArray(_ responseData: Data) throws -> RepositoriesList? {
            do {
                let repositories = try JSONDecoder().decode(RepositoriesList.self, from: responseData)
                return repositories
            } catch {
                fatalError("Could not decode JSON")
            }
        
        return nil
    }
    
    static func parseJsonToPullRequestsArray(_ responseData: Data) throws -> [PullRequest]{
        do {
            let pullRequests = try JSONDecoder().decode([PullRequest].self, from: responseData)
            return pullRequests
        } catch {
            fatalError("Could not decode JSON")
        }
        
        return []
    }
}
