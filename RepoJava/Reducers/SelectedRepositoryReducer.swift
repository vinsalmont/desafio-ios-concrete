//
//  SelectedRepositoryReducer.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 08/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

func selectedRepositoryReducer(action: Action, state: SelectedRepositoryState?) -> SelectedRepositoryState {
    var state = state ?? SelectedRepositoryState()
    
    switch action {
    case let selectAction as SetSelectedRepositoryAction:
        state.selectedRepository = selectAction.selectedRepository
    default:
        break
    }
    
    return state
}
