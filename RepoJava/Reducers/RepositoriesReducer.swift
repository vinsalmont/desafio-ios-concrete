//
//  RepositoriesReducer.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

func repositoriesReducer(action: Action, state: RepositoriesState?) -> RepositoriesState {
    var state = state ?? RepositoriesState(repositories: [], showLoading: false, currentPage: state?.currentPage)
    
    switch action {
    case _ as FetchRepositoriesAction:
        state = RepositoriesState(repositories: state.repositories, showLoading: true, currentPage: state.currentPage)
    case let setCurrentPageAction as SetCurrentPageAction:
        state.currentPage = setCurrentPageAction.currentPage
    case let setRepositoriesAction as SetRepositoriesAction:
        let itemsToAdd = setRepositoriesAction.repositories.filter({ (x) -> Bool in
            return !state.repositories.contains(where: {$0.id == x.id})
        })
        
        state.repositories.append(contentsOf: itemsToAdd)
        state.showLoading = false
    default:
        break
        
    }
    return state
}
