//
//  PullRequestsReducer.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 08/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

func pullRequestsReducer(action: Action, state: PullRequestsState?) -> PullRequestsState {
    var state = state ?? PullRequestsState(pullRequests: [], showLoading: false)
    
    switch action {
    case _ as FetchPullRequestsAction:
        state = PullRequestsState(pullRequests: [], showLoading: true)
    case let setPullRequestsAction as SetPullRequestsAction:
        state.pullRequests = setPullRequestsAction.pullRequests
        state.showLoading = false
    default:
        break
    }
    
    return state
}
