//
//  AppReducer.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

func appReducer(action: Action, state: AppState?) -> AppState {
    return AppState(
        routingState: routingReducer(action: action, state: state?.routingState),
        repositoriesState: repositoriesReducer(action: action, state: state?.repositoriesState),
        pullRequestsState: pullRequestsReducer(action: action, state: state?.pullRequestsState),
        selectedRepositoryState: selectedRepositoryReducer(action: action, state: state?.selectedRepositoryState)
    )
}
