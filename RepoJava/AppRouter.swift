//
//  AppRouter.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

final class AppRouter {
    
    let navigationController: UINavigationController
    
    init(window: UIWindow) {
        navigationController = UINavigationController()
        window.rootViewController = navigationController
        
        store.subscribe(self) {
            $0.select {
                $0.routingState
            }
        }

    }
    
    fileprivate func pushViewController(identifier: String, animated: Bool) {
        let newViewController = instantiateViewController(identifier: identifier)
        let newViewControllerType = type(of: newViewController)
        
        if let currentViewController = navigationController.topViewController {
            let currentType = type(of: currentViewController)

            if currentType == newViewControllerType {
                return
            }
        }
        
        navigationController.pushViewController(newViewController, animated: animated)
    }
    
    private func instantiateViewController(identifier: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
}

// MARK: - StoreSubscriber
extension AppRouter: StoreSubscriber {
    func newState(state: RoutingState) {
        let shouldAnimate = navigationController.topViewController != nil
        pushViewController(identifier: state.navigationState.rawValue, animated: shouldAnimate)
    }
}

// MARK: - Routes
enum RoutingDestination: String {
    case repositoryList = "RepositoryListTableViewController"
    case repositoryDetail = "RepositoryDetailViewController"
}

