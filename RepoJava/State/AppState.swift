//
//  AppState.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

struct AppState: StateType {
    let routingState: RoutingState
    let repositoriesState: RepositoriesState
    let pullRequestsState: PullRequestsState
    let selectedRepositoryState: SelectedRepositoryState
}
