//
//  RepositoriesState.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

struct RepositoriesState: StateType {
    var repositories: [Repository]
    var showLoading: Bool
    var currentPage: Int?
}
