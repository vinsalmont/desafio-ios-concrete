//
//  PullRequest.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 08/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import Foundation

struct PullRequest: Codable {
    let url : String?
    let id : Int?
    let state : String?
    let title : String?
    let user : User?
    let body : String?
    let htmlUrl: String?

    enum CodingKeys: String, CodingKey {
        case url = "url"
        case id = "id"
        case state = "state"
        case title = "title"
        case user = "user"
        case body = "body"
        case htmlUrl = "html_url"
    }
}
