//
//  RepositoriesList.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import Foundation

struct RepositoriesList: Codable {
    let total_count: Int?
    let incomplete_results: Bool?
    let items: [Repository]?
    let message: String?
    let documentationUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case total_count = "total_count"
        case incomplete_results = "incomplete_results"
        case items = "items"
        case message = "message"
        case documentationUrl = "documentation_url"
    }
}
