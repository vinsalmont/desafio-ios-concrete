//
//  FetchPullRequestsAction.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 08/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

func fetchPullRequests(state: AppState, store: Store<AppState>) -> FetchPullRequestsAction {
    
    GithubAPI.loadPullRequests(repositoryPath: (state.selectedRepositoryState.selectedRepository?.full_name!)!) { pullRequests in
        store.dispatch(SetPullRequestsAction(pullRequests: pullRequests))
    }
    
    return FetchPullRequestsAction()
}

struct FetchPullRequestsAction: Action {
}
