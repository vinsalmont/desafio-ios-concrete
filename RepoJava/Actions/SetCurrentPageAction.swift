//
//  SetCurrentPageAction.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 12/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

struct SetCurrentPageAction: Action {
    let currentPage: Int
}

