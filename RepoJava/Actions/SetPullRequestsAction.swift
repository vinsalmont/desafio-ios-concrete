//
//  SetPullRequestsAction.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 08/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

struct SetPullRequestsAction: Action {
    let pullRequests: [PullRequest]
}

