//
//  FetchRepositoriesAction.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift

func fetchJavaRepositories(sate: AppState, store: Store<AppState>) -> FetchRepositoriesAction {
    
    GithubAPI.loadJavaRepositories(page: "\(store.state.repositoriesState.currentPage!)") { repositories in
        if let items = repositories?.items {
            store.dispatch(SetRepositoriesAction(repositories: items))
        }
    }
    
    return FetchRepositoriesAction()
}

struct FetchRepositoriesAction: Action {
}
