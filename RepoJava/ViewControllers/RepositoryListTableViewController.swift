//
//  RepositoryListTableViewController.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 07/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift
import SKActivityIndicatorView

class RepositoryListTableViewController: UITableViewController {

    var tableDataSource: TableDataSource<RepositoryCell, Repository>?
    var lastFetchedIndex = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) {
            $0.select {
                $0.repositoriesState
            }
        }
    
        store.dispatch(RoutingAction(destination: .repositoryList))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        store.dispatch(SetCurrentPageAction(currentPage: 1))
        store.dispatch(fetchJavaRepositories)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        store.dispatch(SetSelectedRepositoryAction(selectedRepository: store.state.repositoriesState.repositories[indexPath.row]))
        store.dispatch(RoutingAction(destination: .repositoryDetail))
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 131
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !store.state.repositoriesState.showLoading){
            store.dispatch(SetCurrentPageAction(currentPage: store.state.repositoriesState.currentPage!+1))
            store.dispatch(fetchJavaRepositories)
        }
    }
    
}

// MARK: - StoreSubscriber
extension RepositoryListTableViewController: StoreSubscriber {
    
    func newState(state: RepositoriesState) {
        tableDataSource = TableDataSource(cellIdentifier: "RepositoryCell", models: state.repositories) { cell, model in
            cell.configureCell(with: model)
            return cell
        }
        
        tableView.dataSource = tableDataSource
        tableView.reloadData()
        
        state.showLoading ? SKActivityIndicator.show() : SKActivityIndicator.dismiss()
    }
}
