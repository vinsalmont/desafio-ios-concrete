//
//  PullRequestViewController.swift
//  RepoJava
//
//  Created by Vinícius Leão Salmont on 08/02/18.
//  Copyright © 2018 Vinícius Leão Salmont. All rights reserved.
//

import ReSwift
import SKActivityIndicatorView

class PullRequestViewController: UIViewController {
    var tableDataSource: TableDataSource<PullRequestCell, PullRequest>?
    var repository: Repository?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var openedIssues: UILabel!
    @IBOutlet weak var closedIssues: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        store.subscribe(self) {
            $0.select {
                $0.pullRequestsState
            }
        }
        
        store.dispatch(RoutingAction(destination: .repositoryDetail))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        store.dispatch(fetchPullRequests)
        title = store.state.selectedRepositoryState.selectedRepository?.name!
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        store.unsubscribe(self)
    }
    
    fileprivate func getOpened() -> Int {
        let openedIssues: [PullRequest] = store.state.pullRequestsState.pullRequests.filter { $0.state == "open" }
        return openedIssues.count
    }
    
    
    fileprivate func getClosed() -> Int {
        let closedIssues: [PullRequest] = store.state.pullRequestsState.pullRequests.filter { $0.state == "closed" }
        return closedIssues.count
    }
    
    fileprivate func populateCounterValues() {
        openedIssues.text = "\(getOpened()) opened"
        closedIssues.text = "\(getClosed()) closed"
    }
    
}

//// MARK: - Table View
extension PullRequestViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 151
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pullRequest = store.state.pullRequestsState.pullRequests[indexPath.row]
        UIApplication.shared.open(URL(string : pullRequest.htmlUrl!)!, options: [:], completionHandler: { (status) in
        })

    }
}

// MARK: - StoreSubscriber
extension PullRequestViewController: StoreSubscriber {
    func newState(state: PullRequestsState) {
        tableDataSource = TableDataSource(cellIdentifier: "PullRequestCell", models: state.pullRequests) { cell, model in
            cell.configureCell(with: model)
            return cell
        }
    
        tableView.dataSource = tableDataSource
        tableView.reloadData()
        
        state.showLoading ? SKActivityIndicator.show() : SKActivityIndicator.dismiss()
        populateCounterValues()
    }
}
